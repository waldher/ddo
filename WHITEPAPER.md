# Transferring Files is Complicated

Ddo is a website, a program, and a mobile app designed to solve the problem of "I want this file moved from A to B, privately, immediately, and without having to set anything up."

## The Problem

Transferring files is still complicated. Every computer or smartphone user has at some point had to creatively move a file from one device to another. With the pervasiveness of computing today, and the proportion of people who own more than one device approaching universal status, the demand for a solution continues to grow. This makes it even more surprising that there is not yet a solution.

They often:

1. Email themselves a file, requiring a web browser, an email account, is usually limited to 10 megabytes, and shares the file with an email provider.
2. Save it to a USB stick and carry it physically, which can move a lot of data quickly, but requires a USB stick to be on hand, doesn't work with most mobile devices, and requires the manual steps of fetching, inserting, moving, and reinserting the USB stick.
3. Upload it online, although this isn't always possible, is often slow, requires an account, and shares it with the file host.
4. Setup a local file share, which requires a device capable of doing so, requires software, requires technical know-how, and does not work for two devices in different locations or networks.

In each of these options, a user must weigh what options are available for their device, is the option compatible with the device that they're using, are they capable enough to setup their device to do so, and are they comfortable with the security and privacy implications of their approach.

Each of these problems is, however, solvable in software, enabling users to send both small and large files as fast as possible, with a minimum of technical know-how.

## The Solution

Ddo is a website, a program, and a mobile app that guarantees a successful, private file transfer from one to one or more devices. Prior setup and prior software installation is not necessary, and there are no limits to the size of the files being transferred.

A user can open the website on a computer and select a file to be sent to their smartphone. On their smartphone, they would open the accompanying app, and can then start receiving the file. If the computer and the smartphone are on the same network, the file will appear for download in the app automatically. If they are not, the smartphone user can type in a temporary code or scan a QR code to start the transfer. This process occurs without the user having to install anything on the computer, without having to setup an account, and without any limits on the size of the file.

Ddo is an open source, free software-as-a-service. Privacy and security are audit-able and guaranteed by the encryption standards in use. The contents of the files transferred are not readable by any computer or device belonging to the user.

## Limitations

Ddo would not be without limitations. The primary focus will be to ensure that files can be transferred at any time without prior setup or file size limits. However, the following limitations will apply:

1. Large file transfers over the internet which use ddo bandwidth will be throttled, sometimes severely. Users can always pay to remove the throttle.
2. Excessive and frequent file transfers over the internet may induce spam-mitigation techniques, such as recaptcha or payment. These limits should be extremely generous, so that even advanced users will not notice them.

## Monetization

In order to be able to afford the operating costs of the servers and bandwidth required to operate the service, some users and file transfers will require payment. Users will be able to pay into a prepaid account, whose balance will be deducted for each megabyte of data transferred. Anonymous users who exceed a reasonable amount of data transferred will begin to see their transfer throttled, and will be offered to submit a prepayment to speed up this and other future transfers.

Operating costs can be kept low by using a cloud provider which makes a fixed amount of bandwidth speed available. Users can then be made to share the available bandwidth on specific "free user" servers. Users with a prepaid account can then use a separate pool of servers with a minimum amount of bandwidth speed available.

### Pricing Examples

A server with a dedicated 1 Gbit/second of bandwidth speed is capable of transferring over 300 TB of data per month for a price of $5. A competitive price for data transfer is $0.1 per gigabyte of outbound data. If this $5 server is fully utilized in a given month, this represents a revenue of $30,000, at a cost of $5.

## Technology

Ddo has three components: An application, a web site, and a web server. File transfers are started from either the application or the web site by selecting a file to transfer. Users will then be given instructions on how to receive the file via either an application running elsewhere, or the web site. The file transfer is then coordinated via the web server, and if necessary, the file is uploaded to and downloaded from the web server.

### Opportunistic Transfer

When using the application, ddo will discover every aspect of the device that may facilitate a transfer. Specifically:

1. What are the device's external and internal IP addresses?
2. Are any devices in the local network trying to send a file?
3. Are any devices in Bluetooth range trying to send a file?
4. Can I punch a hole through the device's firewall?
5. Does the device have internet connectivity?
6. Is the file small enough to fit in a QR code?

Each of these options can be performed without user interaction. If it is discovered that the file transfer can occur over the local network or via Bluetooth, then this will automatically be used. If the devices are not on the same local network, the web server will attempt to punch a hole through the device's firewall. If either of these options succeeds, they will be used for the file transfer in order to provide the fastest speed possible and to save transferring the file over the web server.

If one of the devices is using the web site, Bluetooth will not be available, but the other methods will still be attempted on the other device using the application.

### Fault Tolerance

File sizes will be tracked so that the system can resume failed file transfers. A local storage should be used to track which files were previously transferred, and how much was successful. Devices should then offer to resume a failed file transfer at any time.

Furthermore, the application should automatically fall back on its opportunistic file transfer. If two devices can no longer communicate via the local network, then the file transfer should automatically resume over the internet.

### Privacy and Security

End-to-end encryption is mandatory for any and all file transfers. Upon first transferring a file to a new device, users should first be presented with the public key or a representation of the public key of the other device, which they can then verify manually. This will ensure that the web server does not need to be trusted.

### Application

The application will be available on any desktop computer as a portable (install-free) application, so that a user can get started as quickly as possible. The mobile app will be available on Android and Apple in their respective app stores, and will have just as many features as the desktop application.

Upon opening the application, the application will immediately scan the local network to determine if a device is already trying to send or receive a file. If a device is found, a transfer will be offered immediately. If no device is found, the user will be asked to select a file to transfer.

### Web Site

The web site should use a memorable, short URL, such as ddo.xyz. Users will be immediately presented with a file select field, a submit button, and a receive button. Upon submitting, instructions for receiving the file are presented with multiple options. If the user clicks the receive button, they will be asked for a code to receive the file with.

Barriers to usage must be minimal. Users should not have to click on a cookie warning, and terms of service should be accepted when submitting a file. Given the nature of the files that could be transferred, privacy must be respected such that no identifiable tracking information or metadata shall be collected.
